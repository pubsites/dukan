$(document).ready(function(){
    $('.sidenav').sidenav();
    $('.collapsible').collapsible();
    $('input#input_text, textarea#textarea2').characterCounter();
    $('.materialboxed').materialbox();

    
    const getVendors = ()=>{
      const url = "http://localhost:8000/admin/vendors";
      const vendors = $.get(url);
      vendors.done((data)=>{
        let vendorNames='';
        for (let i=0; i<data.length; i++){
          vendorNames += `<p><a href='#${data[i]._id}' id='${data[i]._id}' class="waves-effect waves-teal btn-flat clc">
          ${data[i].name}
          </a></p>`;         
        }
        $("#vendors").empty().append(vendorNames);
      });
    };
    getVendors();
  
    const dispVendorDetails = ()=>{
      const url = "http://localhost:8000/admin/vendors";
      const vendors = $.get(url);
      vendors.done((data)=>{
        var items = '';
        for(let i=0; i<data.length; i++){
          items += `
            <div class="col s12 m6 l3">
              <div class="card">
                  <div class="card-image waves-effect waves-block waves-light">
                      <img class="activator" src="./images/dukan.png">
                  </div>
                  <div class="card-content">
                      <span class="card-title activator grey-text text-darken-4">${data[i].name}<i class="material-icons right">more_vert</i></span>
                      <p><a href="#">${data[i].email}</a></p>
                  </div>
                  <div class="card-reveal">
                      <span class="card-title grey-text text-darken-4">${data[i].name}<i class="material-icons right">close</i></span>
                      <p>${data[i].address}, ${data[i].email}, ${data[i].phone}, id: ${data[i]._id}</p>
                  </div>
              </div>
            </div> `;
        }

        $("#pgcontent").empty().append(items);
      });
    }

    dispVendorDetails();

    $( "#add_vendor" ).on( "submit", function( event ) {
 
        // Stop form from submitting normally
        event.preventDefault();
       
        // Get some values from elements on the page:
        var $form = $( this ),
          name = $form.find( "input[name='v_name']" ).val(),
          ownerName = $form.find( "input[name='o_name']" ).val(),
          phone = $form.find( "input[name='v_phone']" ).val(),
          email = $form.find( "input[name='v_email']" ).val(),
          password = $form.find( "input[name='v_password']" ).val(),
          address = $form.find( "input[name='v_address']" ).val(),
          url = "http://localhost:8000/admin/vendor";
       
        // Send the data using post
        var posting = $.post( url, { name: name, ownerName: ownerName, phone: phone, 
            email:email, password: password, address: address  },(data)=>{
              
            } );
       
        // Put the results in a div
        posting.done(data => {
           if(data.message){
             myData = `<span>${data.message}</span>`;
             $( "#result" ).empty().append( myData );
           } else{
            //console.log(data);
            $( "#result" ).empty().append( "Saved successfully!" );
           }
           
        } );
      } );

      $("#button").on('click',()=>{
        
        email = $("#email").val(),
        password = $( "#password" ).val(),
        url = "http://localhost:8000/vendor/login";
      // Send the data using post
      var posting = $.post( url, { email:email, password:password});
          posting.done(data=>{
            if(data.name){
              $('#result').css('color','green');
              $('#result').empty().append(" Welcome dear <b>" + data.name + "</b> From "+ data.address);
              $('#signature').empty().append(data.signature)
            }
            else{
              $('#result').css('color','red');
              $('#result').empty().append(data.message);
            }
          });
      });

});